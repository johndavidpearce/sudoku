# Sudoku Game #

Sudoku is a number-based logic game played on a 9x9 grid, where each number in the range 1..9
must fill each row, column and 3x3 box.

This application allows players to interact with a Sudoku board to play Sudoku games interactively
in a terminal, optionally assisting by listing the remaining possible values per cell, row, column
and box. It also supports partially and completely solving the puzzle for you and an undo/redo
stack.

Games a saved in simple text files, e.g.

```
 _ _ _   9 7 _   _ 5 _
 _ _ _   _ _ _   4 7 _
 _ _ 4   _ _ _   8 _ _

 6 _ 5   _ _ _   _ _ _
 _ 8 _   2 _ 7   1 _ _
 _ _ _   1 _ 6   _ _ _

 _ _ 9   _ _ 5   7 _ _
 _ 4 _   _ _ 8   6 _ _
 _ _ 2   _ 9 _   _ _ _
```

Games can be saved and restored from files in this format.
