import re
import curses
import time
from collections import deque, defaultdict
from typing import Tuple, Union, Optional, List, Iterable

from sudoku.undo import UndoStack


def matrix_transpose_2d(matrix):
    """
    Transform a 2D matrix's rows into its columns and vice-versa.
    :param matrix: The matrix to transpose, as an iterable of iterable.
    :return: The matrix's transpose.
    """
    return list(zip(*matrix))


class Sudoku:
    """
    The Sudoku game board, representing a 9x9 grid of numbers where each number in range 1..9 must
    occur in each row, column and 3x3 box.
    """

    def __init__(self, items=None):
        self._board = \
            [[int(v or 0) or None for v in row] for row in items] \
            if items else [[None] * 9 for _ in range(9)]
        self._empty = {(i // 9, i % 9) for i in range(81) if not self[i // 9, i % 9]}
        self._reset_possibles()
        self._actions = UndoStack(self._update, maxlen=1000)

    def _reset_possibles(self):
        self._possibles = [[list(range(1, 10)) for _ in range(9)] for _ in range(9)]
        self._reduce_possibles()

    @property
    def rows(self):
        return self._board

    @property
    def cols(self):
        return list(self.cols_iter())

    def cols_iter(self):
        for col in range(9):
            yield self.col(col)

    def col(self, col):
        return [self[row, col] for row in range(9)]

    def boxes_iter(self):
        for i in range(9):
            yield self.box(i)

    def box(self, box, box_col=None):
        box_r, box_c = self._index(box, box_col, 3)
        if box_r > 2 or box_c > 2:
            raise ValueError(
                'Box indices must be in range 0..2, but got: {}'.format((box_r, box_c)))
        return [
            self._board[box_r * 3 + i // 3][box_c * 3 + (i % 3)]
            for i in range(9)
        ]

    @property
    def boxes(self):
        return list(self.boxes_iter())

    def __iter__(self):
        for row in self.rows:
            yield row

    def __getitem__(self, item: Union[Tuple[int, int], int]) -> Optional[int]:
        row, col = self._index(item)
        return self._board[row][col]

    def __setitem__(self, key: Union[Tuple[int, int], int], value: int):
        row, col = self._index(key)
        self._actions.do((row, col), self._board[row][col], value)

    def _update(self, key, previous_value, new_value):
        row, col = key
        new_value = new_value or None
        if new_value != previous_value:
            self._board[row][col] = new_value
            # undone a previous value --> clear derived state
            if previous_value:
                self._reset_possibles()
            elif new_value:
                self._reduce_possibles(key)
            # update 'empty'
            if previous_value and not new_value:
                self._empty.add(key)
            elif not previous_value and new_value:
                self._empty.remove(key)

    def possibles(self, index, col=None, use_value=False):
        """
        :return: The possible values for a cell, excluding values procluded by row/column/box rules.
        """
        r, c = self._index(index, col)
        v = self._board[r][c] if use_value else None
        return [v] if v else self._possibles[r][c]

    def is_valid_cell(self, index, col=None):
        """
        :return:
            False if the value in this cell conflicts with another in the row/col/box, true
            otherwise.
        """
        r, c = self._index(index, col)
        v = self._board[r][c]
        possibles = self._possibles[r][c]
        return len(possibles) > 0 and (not v or v in possibles)

    def is_valid(self):
        """
        :return: False if any value conflicts with another in the same row/col/box, true otherwise.
        """
        for r in range(9):
            for c in range(9):
                if not self.is_valid_cell(r, c):
                    return False
        return True

    @property
    def is_complete(self):
        return not self._empty

    def reduce(self):
        """
        Perform a single pass of the board to fill cells which have only one possible value,
        as defined by values excluded in the row/column/box.
        :return: Number of results updated.
        """
        count = 0
        with self._actions:
            for r in range(9):
                for c in range(9):
                    if not self[r, c]:
                        values = self.possibles(r, c)
                        if len(values) == 1:
                            self[r, c] = values[0]
                            count += 1
        return count

    def infer(self):
        """
        Perform a single pass of the board to fill cells which have only one possible value, as
        defined by values which must exist in a given row/column/box but cannot occur elsewhere
        in that row/column/box due to each cell having excluded that value.
        :return: Number of results updated.
        """
        # For each row, get remaining values and see if any can only be placed in a single cell
        # within that row. Repeat for columns and boxes.
        count = 0
        with self._actions:
            for cells, group, index in self._walk_constraints():
                for v in getattr(self, 'remaining_' + group)(index):
                    # find the cells in which the value can go
                    positions = []
                    for r, c in cells:
                        if not self[r, c] and v in self.possibles(r, c):
                            positions.append((r, c))
                    if len(positions) == 1:
                        self[positions[0]] = v
                        count += 1
        return count

    def find_naked_pairs(self, max_items=2):
        """
        Perform a single pass of the board, finding situations where naked pairs (pairs of cells
        which can only contain the same two values) prevent values from occurring in other cells in
        the row/cell/box. This only updates the possible values in other cells; it doesn't actually
        find new values.
        :param max_items: optionally specify a higher number to identify naked triplets, etc.
        :return: Number of naked pairs identified which reduced the possible values in other cells.
        """
        pairs = 0
        with self._actions:
            for constrained_cells, _, __ in self._walk_constraints():
                # map cells to possible values
                cell_map = {
                    cell: tuple(self.possibles(cell, use_value=True))
                    for cell in constrained_cells
                }
                # map sets of possible values to the list of cells
                set_map = defaultdict(set)
                for cell, possibles in cell_map.items():
                    set_map[possibles].add(cell)
                # identify naked pairs:
                # value sets which have the same number of possible cells as values (usually two!)
                for possibles, cells in set_map.items():
                    if len(possibles) == len(cells) and 1 < len(cells) <= max_items:
                        # naked pair means that these values must go in these two cells, even though
                        # we don't know which; we can therefore remove the values as possibles from
                        # other cells in the row
                        reduced = False
                        for r, c in constrained_cells:
                            if (r, c) not in cells and not self._board[r][c]:
                                for v in possibles:
                                    if v in self._possibles[r][c]:
                                        self._possibles[r][c].remove(v)
                                        reduced = True
                        if reduced:
                            pairs += 1
        return pairs

    def find_hidden_pairs(self, max_items=2):
        """
        Perform a single pass of the board to discover 'hidden pairs' of values or 'disjoint
        subsets'. Such pairs are combinations where two values may only occur in two possible
        cells within a row/col/box, thus preventing other values from occurring in those same cells.
        :param max_items:
            Optionally specify a larger number to find larger combinations, e.g. naked triplets.
        :return:
            The number of pairs discovered which where used to reduce possible values in one or more
            cell.
        """
        pairs = 0
        with self._actions:
            for cells, _, __ in self._walk_constraints():
                # Find hidden pair:
                # for each value, determine the set of cells in which it can lie
                # for each cell set of length 2, determine if it matches the cell set of another
                # value.
                val_map = defaultdict(set)  # value -> set of cells which can accommodate it
                for cell in cells:
                    for v in self.possibles(cell, use_value=True):
                        val_map[v].add(cell)
                cell_set_map = defaultdict(set)  # cell-sets -> values which can only occur in them
                for v, val_cells in val_map.items():
                    cell_set_map[tuple(sorted(val_cells))].add(v)
                for cell_set, values in cell_set_map.items():
                    if len(cell_set) == len(values) and 1 < len(values) <= max_items:
                        # we now have 2 values which can only occur in 2 locations; remove all
                        # non-participating values from the cells in question
                        reduced = False
                        for cell in cell_set:
                            r, c = cell
                            for v in self._possibles[r][c][:]:
                                if v not in values:
                                    self._possibles[r][c].remove(v)
                                    reduced = True
                        pairs += reduced
        return pairs

    def xwing(self):
        patterns = 0
        # Pre-calculate which values can occur in which columns for each row
        row_val_map = []  # [value -> col set]
        for r in range(9):
            val_map = defaultdict(set)
            for c in range(9):
                if not self[r, c]:
                    for v in self.possibles(r, c, use_value=True):
                        val_map[v].add(c)
            row_val_map.append(val_map)
        # Find any values with matching columns across multiple rows
        for r1, val_map in enumerate(row_val_map):
            for v, cols in val_map.items():
                if len(cols) == 2:
                    for r2, val_map2 in enumerate(row_val_map):
                        if r1 != r2 and val_map2[v] == cols:
                            # X-wing pattern found: we now know 'v' can only be in column
                            # 'cols' in both rows 'r1' and 'r2'; this value can now be removed from
                            # all other cells in both rows and both columns.
                            eliminated = False
                            for r in (r1, r2):
                                for c in range(9):
                                    if c not in cols and not self[r, c] and \
                                            v in self._possibles[r][c]:
                                        self._possibles[r][c].remove(v)
                                        eliminated = True
                            for c in cols:
                                for r in range(9):
                                    if r not in (r1, r2) and not self[r, c] and \
                                            v in self._possibles[r][c]:
                                        self._possibles[r][c].remove(v)
                                        eliminated = True
                            if eliminated:
                                patterns += 1
        return patterns

    def solve(self):
        """
        Attempt to solve the puzzle using logic, as a human would.
        Returns success, along with metrics indicating the puzzle difficulty.
        :return: A tuple of (success, cells updated, cells reduced, cells inferred, infer passes)
        """
        total_reduced = 0
        total_inferred = 0
        infer_passes = 0
        total_naked_pairs = 0
        total_hidden_pairs = 0
        total_xwing_patterns = 0
        with self._actions:
            while self._empty:
                reduced = self.reduce()
                total_reduced += reduced
                if reduced:
                    continue
                inferred = self.infer()
                total_inferred += inferred
                if inferred:
                    infer_passes += 1
                    continue
                naked_pairs = self.find_naked_pairs()
                total_naked_pairs += naked_pairs
                if naked_pairs:
                    continue
                hidden_pairs = self.find_hidden_pairs()
                total_hidden_pairs += hidden_pairs
                if hidden_pairs:
                    continue
                xwing_patterns = self.xwing()
                total_xwing_patterns += xwing_patterns
                if xwing_patterns:
                    continue
                break  # failed to solve
        return (
            # success, total changes
            not self._empty, total_reduced + total_inferred,
            # difficulty metrics
            total_reduced, total_inferred, infer_passes)  # TODO: include pairs & xwing

    def brute_solve(self):
        # TODO: solve using brute force and a depth-first search
        pass

    def unsolve(self):
        # TODO: unsolve: remove values until just before unsolvable
        pass

    def remaining_row(self, row):
        """
        :return: The possible values remaining for a row, excluding values already in that row.
        """
        values = self.rows[row]
        return [v for v in range(1, 10) if v not in values]

    def remaining_col(self, col):
        """
        :return:
            The possible values remaining for a column, excluding values already in that column.
        """
        values = self.col(col)
        return [v for v in range(1, 10) if v not in values]

    def remaining_box(self, box, box_col=None):
        """
        :param box: The box index, numbered 1-9 by row then column.
        :return: The possible values remaining for a box, excluding values already in that box.
        """
        values = self.box(box, box_col)
        return [v for v in range(1, 10) if v not in values]

    @staticmethod
    def _index(
            item: Union[Tuple[int, int], int], col: int = None, size: int = 9) \
            -> Tuple[int, int]:
        if col is not None:
            return item, col
        try:
            row, col = item
        except TypeError:
            row = item // size
            col = item % size
        return row, col

    def _reduce_possibles(self, change_pos: Tuple[int, int] = None):
        count = 0
        ref_r, ref_c = change_pos if change_pos else (None, None)
        for r in range(9):
            for c in range(9):
                if not change_pos or r == ref_r or c == ref_c \
                        or (r // 3, c // 3) == (ref_r // 3, ref_c // 3):
                    row = self.rows[r][:]
                    row.pop(c)
                    col = self.col(c)
                    col.pop(r)
                    box = self.box(r // 3, c // 3)
                    box.pop(r % 3 * 3 + c % 3)
                    for v in self._possibles[r][c][:]:
                        if v in row \
                                or v in col \
                                or v in box:
                            self._possibles[r][c].remove(v)
                            count += 1
        return count

    @staticmethod
    def _walk_constraints() -> Iterable[Tuple[List[Tuple[int, int]], str, int]]:
        """
        Perform a 'walk' of all the constrained sets of cells: rows, columns and boxes.
        Results are the (row, col) coordinates, not the values.
        :return: An iterable of each set of constrained cells as a list of (row, col).
        """
        for r in range(9):
            yield [(r, c) for c in range(9)], 'row', r
        for c in range(9):
            yield [(r, c) for r in range(9)], 'col', c
        for b in range(9):
            yield [(b // 3 * 3 + i // 3, b % 3 * 3 + i % 3) for i in range(9)], 'box', b

    def export(self):
        """
        Export the board as a string.
        :return: The board represented as a human and machine readable string.
        """
        output = ''
        for r, row in enumerate(self.rows):
            if r % 3 == 0 and r > 0:
                output += '\n'
            for c, val in enumerate(row):
                output += '{gap} {}'.format(val or '_', gap='  ' if c % 3 == 0 and c > 0 else '')
            output += '\n'
        return output

    __str__ = export
    __repr__ = export

    def undo(self):
        return len(self._actions.undo() or [])

    def redo(self):
        return len(self._actions.redo() or [])

    def __copy__(self):
        return Sudoku(self._board)

    def randomize(self):
        # TODO: randomize: transpose (rows->cols), translate (map numbers), shuffle rows/cols/boxes
        pass

    def __eq__(self, other):
        return hasattr(other, 'rows') and other.rows == self.rows

    @staticmethod
    def parse(board_string: str):
        """
        Construct a Sudoku board by parsing the given string, e.g. that returned by `export()`.
        :param board_string:
            The board, represented as a row per line and '_' as placeholders. Whitespace is ignored.
        :return: The constructed Sudoku board.
        """
        return Sudoku([
            re.sub(r'\s+', '', row).replace('_', '0')
            for row in board_string.split('\n') if row.strip()])

    @staticmethod
    def generate_solved():
        # TODO: generate solved
        pass


class Game:
    """
    An interactive Sudoku game.
    """

    def __init__(self, display_screen, save_path, board=None, cursor=(0, 0)):
        self._screen = display_screen
        self._board = board or Sudoku()
        self._cursor = cursor
        self._file = None
        self._save_path = save_path
        self._dirty = False
        self._display_info = False
        self._messages = deque(maxlen=3)

    def render(self):
        """
        Display the game on screen.
        """
        self._screen.clear()
        y, x = 1, 1
        max_y, max_x = self._screen.getmaxyx()
        for r in range(9):
            # values
            for c in range(9):
                color = 0
                if (r, c) == self._cursor:
                    color += 1  # highlighted
                if not self._board.is_valid_cell(r, c):
                    color += 2  # error
                self._screen.addstr(
                    y + r + r // 3, x + c * 2 + c // 3,
                    str(len(self._board.possibles(r, c)))
                    if self._display_info else str(self._board[r, c] or '_'),
                    curses.color_pair(color))
            # row remaining
            self._screen.addstr(
                y + r + r // 3, x + 21, ''.join(str(v) for v in self._board.remaining_row(r)))
        # column remaining
        for c in range(9):
            for i, v in enumerate(self._board.remaining_col(c)):
                self._screen.addstr(y + 12 + i, x + c * 2 + c // 3, str(v))
        # box remaining
        for b_r in range(3):
            for b_c in range(3):
                b = b_r * 3 + b_c
                values = self._board.remaining_box(b)
                for r in range(3):
                    for c in range(3):
                        self._screen.addstr(
                            y + b_r * 4 + r,
                            21 + 11 + x + b_c * 4 + c,
                            str(values[r * 3 + c]) if len(values) > r * 3 + c else '.')
        # cell possible values
        self._screen.addstr(y + 12, x + 22, '({},{})'.format(*(v + 1 for v in self._cursor)))
        self._screen.addstr(
            y + 13, x + 22, ''.join(str(v) for v in self._board.possibles(self._cursor)))
        # messages
        for i, message in enumerate(self.messages):
            self._screen.addstr(max_y - 2 - i, 0, message)
        # help text
        self._screen.addstr(
            max_y - 1, 0,
            '[q]uit, [s]ave, [arrow] move, [0-9] update, [?] board info, '
            '[r]educe, [i]nfer, [c] solve, '
            '[z] undo, [Z] redo'[:max_x - 2])
        self._screen.refresh()

    def get_key(self):
        """
        Block until a single key is read.
        :return: True if the screen was updated, false otherwise.
        """
        render = True
        key = self._screen.getch()
        if key == curses.KEY_UP and self._cursor[0] > 0:
            self._cursor = self._cursor[0] - 1, self._cursor[1]
        elif key == curses.KEY_DOWN and self._cursor[0] < 8:
            self._cursor = self._cursor[0] + 1, self._cursor[1]
        elif key == curses.KEY_LEFT and self._cursor[1] > 0:
            self._cursor = self._cursor[0], self._cursor[1] - 1
        elif key == curses.KEY_RIGHT and self._cursor[1] < 8:
            self._cursor = self._cursor[0], self._cursor[1] + 1
        elif ord('0') <= key <= ord('9'):
            self._board[self._cursor[0], self._cursor[1]] = key - ord('0')
            self._dirty = True
        elif key == curses.KEY_BACKSPACE:
            self._board[self._cursor[0], self._cursor[1]] = 0
            self._dirty = True
        elif key == ord('r'):
            reduced = self._board.reduce()
            if reduced:
                self._dirty = True
                self._message('Reduce: {} values filled'.format(reduced))
            else:
                self._message('Reduce: no candidates found')
        elif key == ord('i'):
            inferred = self._board.infer()
            if inferred:
                self._dirty = True
                self._message('Infer: {} values filled'.format(inferred))
            else:
                self._message('Infer: no candidates found')
        elif key == ord('c'):
            solved, changed, reduced, inferred, infer_passes = self._board.solve()
            if changed:
                self._dirty = True
            self._message(
                '{} with {} changes: {} reduced, {} inferred ({} passes)'.format(
                    'Solved' if solved else 'Failed to solve',
                    changed, reduced, inferred, infer_passes))
        elif key == ord('z'):
            changes = self._board.undo()
            if changes:
                self._dirty = True
                self._message('Undone {} changes'.format(changes))
            else:
                self._message('No changes to undo')
        elif key == ord('Z'):
            changes = self._board.redo()
            if changes:
                self._dirty = True
                self._message('Redone {} changes'.format(changes))
            else:
                self._message('No changes to redo')
        elif key == ord('q'):
            raise KeyboardInterrupt
        elif key == ord('s'):
            self.save()
            self._message('Saved: {}'.format(self._save_path))
        elif key == ord('?'):
            self._display_info = not self._display_info
        elif key == curses.ERR:
            pass  # key input timeout; we want to render in this case
        else:
            render = False
        if render:
            self.render()
        return render

    @property
    def dirty(self):
        """
        :return: True if the board has been updated since the last save.
        """
        return self._dirty

    def save(self):
        """
        Save the game state to file.
        """
        if not self._file:
            self._file = open(self._save_path, 'w')
        self._file.seek(0)
        self._file.write(self._board.export())
        self._file.truncate()
        self._dirty = False

    def close(self):
        """
        Close the game file.
        """
        if self._file:
            self._file.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def _message(self, message, timeout=3):
        self._messages.appendleft((time.time() + timeout, message))

    @property
    def messages(self):
        now = time.time()
        while self._messages and self._messages[-1][0] < now:
            self._messages.pop()
        return [message for expiry, message in self._messages]
