#!/usr/bin/env python3
import argparse
import os
import sys
import curses
from datetime import datetime
from contextlib import contextmanager

from sudoku.sudoku import Sudoku, Game


@contextmanager
def screen():
    """
    Manages a curses screen, restoring state upon exit.
    :return: The screen object.
    """
    std_scr = curses.initscr()
    curses.noecho()  # don't display typed chars
    # curses.cbreak()  # enable reacting to keys without enter press
    curses.halfdelay(3)  # enable reacting to keys without enter press, but don't block long
    curses.start_color()
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)  # highlighted
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)  # error
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_RED)  # error + highlighted
    std_scr.keypad(True)  # simplify reading keys
    try:
        yield std_scr
    finally:
        curses.nocbreak()
        std_scr.keypad(False)
        curses.echo()
        curses.endwin()


def main(arg_list=None):
    arg_parser = argparse.ArgumentParser(description='Sudoku interactive game and solver')
    arg_parser.add_argument('-f', '--load', help='Load a saved game')
    args = arg_parser.parse_args(arg_list)

    save_path = args.load or 'sudoku-{}.sav'.format(
        datetime.now().replace(microsecond=0).isoformat().replace('-', '').replace(':', ''))
    auto_save_path = save_path.replace('.sav', '') + '.autosave'

    if os.path.exists(save_path):
        with open(save_path, 'r') as f:
            sudoku = Sudoku.parse(f.read())
    else:
        sudoku = Sudoku()

    with \
            screen() as display, \
            open(auto_save_path, 'w') as game_file, \
            Game(display, save_path, sudoku) as game:
        try:
            game.render()
            while True:
                game.get_key()
                # auto save
                if game.dirty:
                    game_file.seek(0)
                    game_file.write(sudoku.export())
                    game_file.truncate()
        except KeyboardInterrupt:
            pass

    if not game.dirty:
        os.remove(auto_save_path)

    print(sudoku)


if __name__ == '__main__':
    sys.exit(main())
