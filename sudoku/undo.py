from collections import deque


class UndoStack:
    """
    Keep track of actions performed, so they can be undone/redone.
    """

    def __init__(self, apply, maxlen=100):
        """
        :param apply:
            A function taking arguments (key, before, after) which will cause the action to take
            effect.
        :param maxlen: The maximum number of actions (or transactions) to keep in the stack.
        """
        self._apply_fn = apply
        self._max_len = maxlen
        self._undo_stack = deque(maxlen=maxlen)
        self._redo_stack = deque(maxlen=maxlen)
        self._transaction = None
        self._transactions = 0

    def undo(self):
        """
        Undo the previous action (or actions in the case of a transaction), if any.
        :return: The actions undone, or none.
        """
        if len(self._undo_stack):
            actions = self._undo_stack.pop()
            self._apply(actions, reverse=True)
            self._redo_stack.append(actions)
            return actions

    def redo(self):
        """
        Redo the previously undone action(s), if any.
        :return: The redone actions, or none.
        """
        if len(self._redo_stack):
            actions = self._redo_stack.pop()
            self._apply(actions)
            self._undo_stack.append(actions)
            return actions

    def do(self, key, before, after):
        """
        Apply an action, add it to the undo stack, and clear the redo stack.
        :param key: The scope of the action, to define the target; the thing being changed.
        :param before: The previous value, before the change.
        :param after: The new value, after the change.
        """
        action = (key, before, after)
        self._apply([action])
        if self._transaction is not None:
            self._transaction.append(action)
        else:
            self._undo_stack.append([action])
            self._redo_stack.clear()

    __call__ = do

    def _apply(self, actions, reverse=False):
        if reverse:
            actions = reversed(actions)
        for key, before, after in actions:
            if reverse:
                self._apply_fn(key, after, before)
            else:
                self._apply_fn(key, before, after)

    def __enter__(self):
        """
        Start a reentrant transaction.
        That is, begin a set of actions which should all be undone as one.
        Keep track of transactions started with a counter, as a single transaction can itself
        contain sub-transactions, which will all be grouped together in a single transaction.
        """
        if not self._transactions:
            self._transaction = deque(maxlen=self._max_len)
        self._transactions += 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._transactions -= 1
        if not self._transactions:
            if self._transaction:
                self._undo_stack.append(self._transaction)
                self._redo_stack.clear()
            self._transaction = None
