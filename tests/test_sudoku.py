from copy import copy

import pytest
from pytest import fixture
from sudoku.sudoku import Sudoku


@fixture(name='boards')
def _boards():
    boards = {}
    boards['medium'] = (
        """
         _ _ _   9 7 _   _ 5 _
         _ _ _   _ _ _   4 7 _
         _ _ 4   _ _ _   8 _ _

         6 _ 5   _ _ _   _ _ _
         _ 8 _   2 _ 7   1 _ _
         _ _ _   1 _ 6   _ _ _

         _ _ 9   _ _ 5   7 _ _
         _ 4 _   _ _ 8   6 _ _
         _ _ 2   _ 9 _   _ _ _
        """,
        """
         1 3 8   9 7 4   2 5 6
         2 5 6   8 1 3   4 7 9
         7 9 4   5 6 2   8 3 1

         6 1 5   4 8 9   3 2 7
         9 8 3   2 5 7   1 6 4
         4 2 7   1 3 6   9 8 5

         8 6 9   3 4 5   7 1 2
         5 4 1   7 2 8   6 9 3
         3 7 2   6 9 1   5 4 8
        """
    )
    boards['easy'] = (
        """
         1 3 _   9 7 4   2 5 _
         2 _ _   _ _ _   4 7 _
         7 _ 4   _ _ 2   8 _ _

         6 1 5   _ _ 9   3 2 7
         9 8 3   2 5 7   1 _ _
         4 2 7   1 3 6   9 8 5

         _ 6 9   _ _ 5   7 _ _
         5 4 1   7 _ 8   6 _ _
         _ 7 2   6 9 _   5 _ _
        """,
        boards['medium'][1]
    )
    # https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques
    boards['naked_pairs_in_box'] = (
        """
        4 _ _  8 _ 9  1 _ _
        _ _ 7  _ _ _  _ 9 _
        9 5 _  _ 2 _  _ _ 7
        1 _ _  _ 9 _  _ _ 3
        3 9 2  4 _ 7  8 _ _
        6 _ _  _ 3 _  _ _ 9
        7 2 _  _ 8 _  _ 6 _
        _ 1 _  _ _ _  2 _ _
        _ _ 3  1 _ 2  _ _ 4
        """,
        None
    )
    # https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques
    boards['naked_pairs_row_col'] = (
        """
        9 6 _  _ 1 _  _ 3 _
        3 _ 2  _ _ _  8 _ 4
        _ 7 _  _ _ _  _ 9 6
        _ _ _  3 _ 8  _ _ _
        6 _ 9  _ _ _  _ 8 5
        _ _ _  4 _ 9  _ _ _
        _ 2 _  5 8 4  _ 6 _
        5 _ 8  _ _ _  2 _ 7
        _ 4 _  _ 9 _  3 5 _
        """,
        None
    )
    # https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques
    boards['hidden_pairs'] = (
        """
        4 _ _  3 1 9  _ _ 6
        _ _ 1  _ _ _  9 _ _
        _ 6 7  4 _ _  _ 2 1
        7 _ _  _ 5 _  _ _ 4
        _ _ _  1 4 2  _ _ _
        2 _ _  _ 7 _  _ _ 8
        _ 2 _  _ _ _  _ 6 _
        _ _ 4  _ _ _  8 _ _
        1 _ _  5 _ 8  _ _ 7
        """,
        None
    )
    # https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques
    boards['xwing'] = (
        """
        _ _ 5  _ _ 4  _ _ _
        _ _ _  _ 6 _  _ 9 _
        3 _ _  _ _ _  _ _ 7
        _ _ _  _ 4 _  _ _ _
        _ _ 8  _ _ _  4 _ _
        5 4 1  _ _ _  _ _ 9
        2 _ _  _ _ _  _ _ 3
        _ _ 7  4 _ _  _ _ _
        _ _ _  _ _ 3  _ _ _
        """,
        None
    )
    # https://www.researchgate.net/publication/264572573_Sudoku_Puzzle_Complexity#pf1
    boards['researchgate'] = (
        """
        _ _ _  _ _ _  2 _ _
        _ 8 _  _ _ 7  _ 9 _
        6 _ 2  _ _ _  5 _ _
        _ 7 _  _ 6 _  _ _ _
        _ _ _  9 _ 1  _ _ _
        _ _ _  _ 2 _  _ 4 _
        _ _ 5  _ _ _  6 _ 3
        _ 9 _  4 _ _  _ 7 _
        _ _ 6  _ _ _  _ _ _
        """,
        """
        9 5 7  6 1 3  2 8 4
        4 8 3  2 5 7  1 9 6
        6 1 2  8 4 9  5 3 7
        1 7 8  3 6 4  9 5 2
        5 2 4  9 7 1  3 6 8
        3 6 9  5 2 8  7 4 1
        8 4 5  7 9 2  6 1 3
        2 9 1  4 3 6  8 7 5
        7 3 6  1 8 5  4 2 9
        """
    )
    return boards


@fixture(name='sample')
def _sample(boards):
    return boards['easy'][0]


class TestSudoku:
    def test_init_empty(self):
        assert Sudoku().rows == [[None] * 9] * 9

    def test_init_not_empty(self):
        rows = \
            [[1, 2, 3, 0, 0, 0, 0, 0, 9]] + \
            [[0] * 9] * 7 + \
            [[9, 0, 0, 0, 0, 0, 0, 0, 0]]
        board = Sudoku(rows)
        assert board.rows == [[v or None for v in r] for r in rows]

        # check rows were copied, not referenced
        rows[0][0] = 6
        assert board.rows != rows

    def test_parse(self, sample):
        board = Sudoku.parse(sample)
        _ = None
        assert board.rows == [
            [1, 3, _, 9, 7, 4, 2, 5, _],
            [2, _, _, _, _, _, 4, 7, _],
            [7, _, 4, _, _, 2, 8, _, _],
            [6, 1, 5, _, _, 9, 3, 2, 7],
            [9, 8, 3, 2, 5, 7, 1, _, _],
            [4, 2, 7, 1, 3, 6, 9, 8, 5],
            [_, 6, 9, _, _, 5, 7, _, _],
            [5, 4, 1, 7, _, 8, 6, _, _],
            [_, 7, 2, 6, 9, _, 5, _, _]
        ]

    def test_export(self, sample):
        board = Sudoku.parse(sample)
        assert board.export() == ' ' + """
 1 3 _   9 7 4   2 5 _
 2 _ _   _ _ _   4 7 _
 7 _ 4   _ _ 2   8 _ _

 6 1 5   _ _ 9   3 2 7
 9 8 3   2 5 7   1 _ _
 4 2 7   1 3 6   9 8 5

 _ 6 9   _ _ 5   7 _ _
 5 4 1   7 _ 8   6 _ _
 _ 7 2   6 9 _   5 _ _
""".strip() + '\n'

    def test_copy(self, sample):
        board1 = Sudoku.parse(sample)
        board1_copy = copy(board1)
        assert board1 == board1_copy
        board1[0, 0] = 9
        assert board1 != board1_copy

    def test_possibles(self, sample):
        """Possible values upon init"""
        board = Sudoku.parse(sample)
        # box 1 (scanning columns)
        assert board.possibles(0, 2) == [6, 8]
        assert board.possibles(1, 2) == [6, 8]
        assert board.possibles(1, 1) == [5, 9]
        assert board.possibles(2, 1) == [5, 9]
        # middle box (scanning row)
        assert board.possibles(3, 3) == [4, 8]
        assert board.possibles(3, 4) == [4, 8]
        # top-right
        assert board.possibles(0, 8) == [6]
        # bottom-right
        assert board.possibles(8, 8) == [1, 3, 4, 8]

    def test_possibles_filled_cell(self, sample):
        """
        Filled cells should still have their possible values computed as if they were not filled.
        """
        board = Sudoku.parse(sample)
        assert board.possibles(0, 0) == [1, 8]

    def test_possibles_update(self, sample):
        """
        Updating the board should update the possible values.
        """
        board = Sudoku.parse(sample)
        board[0, 2] = 6
        assert board.possibles(1, 2) == [8]
        board.undo()
        assert board.possibles(1, 2) == [6, 8]

    def test_remaining_row_col_box(self, sample):
        board = Sudoku.parse(sample)
        assert board.remaining_row(0) == [6, 8]
        assert board.remaining_row(5) == []
        assert board.remaining_col(0) == [3, 8]
        assert board.remaining_col(6) == []
        assert board.remaining_box(0) == [5, 6, 8, 9]
        assert board.remaining_box(3) == []

    def test_remaining_row_col_box_updates(self, sample):
        board = Sudoku.parse(sample)
        board[0, 8] = 6
        assert board.remaining_row(0) == [8]

    def test_is_valid_cell(self, sample):
        board = Sudoku.parse(sample)
        assert board.is_valid_cell(0, 8)
        board[0, 8] = 6
        assert board.is_valid_cell(0, 8)
        board[0, 8] = 8
        assert not board.is_valid_cell(0, 8)
        board[0, 8] = None
        assert board.is_valid_cell(0, 8)

    def test_is_valid(self, sample):
        board = Sudoku.parse(sample)
        assert board.is_valid()
        board[0, 8] = 6
        assert board.is_valid()
        board[0, 8] = 8
        assert not board.is_valid()
        board[0, 8] = None
        assert board.is_valid()

    def test_reduce(self, sample):
        board = Sudoku.parse(sample)
        assert board.reduce() == 3
        expected = Sudoku.parse(sample)
        expected[0, 8] = 6
        expected[4, 8] = 4
        expected[7, 4] = 2
        assert board == expected

    def test_infer(self, sample):
        board = Sudoku.parse(sample)
        assert board.infer() == 6
        expected = Sudoku.parse(sample)
        # inferred by row
        expected[0, 2] = 8
        expected[0, 8] = 6  # only place it can go in row (last number remaining in row)
        expected[1, 2] = 6
        expected[2, 4] = 6
        expected[4, 7] = 6
        expected[4, 8] = 4
        assert board == expected

    def test_naked_pairs_in_box(self, boards):
        board = Sudoku.parse(boards['naked_pairs_in_box'][0])
        # assert board.possibles(8, 1) == [6, 8]
        assert board.possibles(6, 2) == [4, 5, 9]
        assert board.possibles(7, 2) == [4, 5, 6, 8, 9]
        # assert board.find_naked_pairs() > 0  # TODO: test naked pairs
        assert board.find_hidden_pairs() > 0
        # thanks to the pair of 4-9 values, other values can be removed from these boxes
        assert board.possibles(6, 2) == [4, 9]
        assert board.possibles(7, 2) == [4, 9]
        # inference: the only place for 6 in the box is 8,1
        # assert board.possibles(8, 1) == [6]

    def test_naked_pairs_in_row_cols(self, boards):
        # https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques
        board = Sudoku.parse(boards['naked_pairs_row_col'][0])
        # assert board.possibles(8, 2) == [1, 6, 7]
        assert board.possibles(8, 3) == [1, 2, 6, 7]
        assert board.possibles(8, 5) == [1, 2, 6, 7]
        # assert board.find_naked_pairs() > 0  # TODO: test naked pairs
        assert board.find_hidden_pairs() > 0
        assert board.possibles(8, 3) == [2, 7]
        assert board.possibles(8, 5) == [2, 7]
        # assert board.possibles(8, 2) == [6]

    @pytest.mark.skip(reason='Not yet implemented')
    def test_naked_triplets(self, sample):
        pass

    def test_hidden_pairs(self, boards):
        # https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques
        board = Sudoku.parse(boards['hidden_pairs'][0])
        for c in range(9):
            print('{} -> {}'.format(c, board.possibles(6, c, use_value=True)))
        assert board.possibles(6, 5) == [1, 3, 4, 7]
        assert board.possibles(6, 6) == [1, 3, 4, 5]
        assert board.find_hidden_pairs() > 0
        assert board.possibles(6, 5) == [1, 4]
        assert board.possibles(6, 6) == [1, 4]

    # @pytest.mark.skip(reason='Not yet implemented')
    def test_xwing(self, boards):
        # https://www.conceptispuzzles.com/index.aspx?uri=puzzle/sudoku/techniques
        board = Sudoku.parse(boards['xwing'][0])
        assert board.possibles(1, 2) == [2, 4]
        assert board.xwing() > 0
        assert board.possibles(1, 2) == [2]

    def test_solve_basic(self, boards):
        problem, solution = boards['easy']
        board = Sudoku.parse(problem)
        assert not board.is_complete
        assert board.is_valid()
        assert board.solve() == (True, 29, 15, 14, 1)
        assert board == Sudoku.parse(solution)
        assert board.is_valid()
        assert board.is_complete

    @pytest.mark.parametrize('name', [
        'easy', 'medium', 'naked_pairs_in_box', 'naked_pairs_row_col', 'hidden_pairs'])
    def test_solve_more(self, boards, name):
        puzzle, solution = boards[name]
        board = Sudoku.parse(puzzle)
        result = board.solve()
        assert result[0], '{} not solved: {}\n{}'.format(name, result, board)
        assert board.is_complete
        assert board.is_valid()

    def test_undo_move(self, sample):
        board = Sudoku.parse(sample)
        assert board.is_valid()
        board[0, 8] = 8
        assert not board.is_valid()
        assert board.undo() == 1
        assert board[0, 8] is None
        assert board.is_valid()
        assert board.undo() == 0

    def test_undo_solve(self, sample):
        board = Sudoku.parse(sample)
        assert not board.is_complete
        board.solve()
        assert board.is_complete
        assert board.undo() == 29
        assert not board.is_complete
        assert board == Sudoku.parse(sample)

    def test_redo(self):
        board = Sudoku()
        board[0, 0] = 4
        board[0, 0] = 6
        assert board[0, 0] == 6
        assert board.undo() == 1
        assert board[0, 0] == 4
        assert board.undo() == 1
        assert board[0, 0] is None
        assert board.undo() == 0
        assert board.redo() == 1
        assert board[0, 0] == 4
        assert board.redo() == 1
        assert board[0, 0] == 6
        assert board.redo() == 0
