from unittest.mock import Mock, call

from sudoku.undo import UndoStack


class TestUndoStack:
    def test_do(self):
        do = Mock()
        stack = UndoStack(do)
        stack.do('abc', 'start', 'updated')
        do.assert_called_once_with('abc', 'start', 'updated')

    def test_undo(self):
        do = Mock()
        stack = UndoStack(do)
        action = 'abc', 'start', 'updated'
        stack.do(*action)
        assert stack.undo() == [action]
        assert do.call_args_list == [
            call(*action), call('abc', 'updated', 'start')]

    def test_undo_multiple(self):
        do = Mock()
        stack = UndoStack(do)
        stack.do('abc', 'start', 'updated')
        stack.do('1', '2', '3')
        assert stack.undo() == [('1', '2', '3')]
        assert stack.undo() == [('abc', 'start', 'updated')]
        assert stack.undo() is None
        assert do.call_args_list == [
            call('abc', 'start', 'updated'),
            call('1', '2', '3'),
            call('1', '3', '2'),
            call('abc', 'updated', 'start'),
        ]

    def test_redo(self):
        do = Mock()
        stack = UndoStack(do)
        stack.do('abc', 'start', 'updated')
        stack.do('1', '2', '3')
        assert stack.redo() is None
        assert stack.undo() == [('1', '2', '3')]
        assert stack.redo() == [('1', '2', '3')]
        assert stack.redo() is None
        assert stack.undo() == [('1', '2', '3')]
        assert stack.undo() == [('abc', 'start', 'updated')]
        assert stack.undo() is None
        assert stack.redo() == [('abc', 'start', 'updated')]
        assert do.call_args_list == [
            call('abc', 'start', 'updated'),
            call('1', '2', '3'),
            call('1', '3', '2'),
            call('1', '2', '3'),  # redo
            call('1', '3', '2'),  # re-undo
            call('abc', 'updated', 'start'),  # undo x2
            call('abc', 'start', 'updated'),  # redo
        ]

    def test_do_clears_redo_stack(self):
        do = Mock()
        stack = UndoStack(do)
        stack.do('abc', 'start', 'updated')
        stack.undo()
        stack.do('1', '2', '3')
        assert stack.redo() is None
        assert do.call_args_list == [
            call('abc', 'start', 'updated'),
            call('abc', 'updated', 'start'),
            call('1', '2', '3')
        ]

    def test_transaction_undo(self):
        do = Mock()
        stack = UndoStack(do)
        stack.do('abc', 'start', 'updated')
        with stack:
            stack.do('1', '2', '3')
            stack.do('4', '5', '6')
        assert list(stack.undo()) == [('1', '2', '3'), ('4', '5', '6')]
        assert do.call_args_list == [
            call('abc', 'start', 'updated'),
            call('1', '2', '3'),
            call('4', '5', '6'),
            call('4', '6', '5'),
            call('1', '3', '2'),
        ]
        assert list(stack.redo()) == [('1', '2', '3'), ('4', '5', '6')]
        assert do.call_args_list[-2:] == [
            call('1', '2', '3'),
            call('4', '5', '6'),
        ]

    def test_transaction_nested(self):
        do = Mock()
        stack = UndoStack(do)
        stack.do('abc', 'start', 'updated')
        with stack:
            stack.do('1', '2', '3')
            with stack:
                stack.do('4', '5', '6')
        assert list(stack.undo()) == [('1', '2', '3'), ('4', '5', '6')]
        assert do.call_args_list == [
            call('abc', 'start', 'updated'),
            call('1', '2', '3'),
            call('4', '5', '6'),
            call('4', '6', '5'),
            call('1', '3', '2'),
        ]

    def test_max_len(self):
        do = Mock()
        stack = UndoStack(do, maxlen=3)
        for i in range(5):
            stack.do(1, 2, 3)
        with stack:
            for i in range(5):
                stack.do(4, 5, 6)
        assert list(stack.undo()) == [(4, 5, 6)] * 3
        assert stack.undo() == [(1, 2, 3)]
        assert stack.undo() == [(1, 2, 3)]
        assert stack.undo() is None
