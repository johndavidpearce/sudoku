import os

from setuptools import setup, find_packages


def read(*paths):
    """Build a file path from *paths* and return the contents."""
    with open(os.path.join(*paths), 'r') as f:
        return f.read()


def get_version():
    properties = {}
    exec(read('sudoku/__properties__.py'), properties)
    return properties['__version__']


setup(

    name="sudoku-game",
    version=get_version(),

    description="Interactive Sudoku game and solver",
    long_description=read("README.md"),

    url="http://bitbucket.org/johndavidpearce/sudoku/",
    author="John Pearce",
    author_email="john.pearce@rocketboots.com",

    packages=find_packages(exclude=["tests*"]),
    include_package_data=True,

    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7'
    ],

    # Runtime dependencies
    python_requires='>=3.3, <4',

    # Testing
    setup_requires=['pytest-runner'],
    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-pep8'
    ],

    entry_points={
        'console_scripts': [
            'sudoku=sudoku.__main__:main',
        ]
    }
)
